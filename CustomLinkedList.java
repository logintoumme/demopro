package com.testapplication.list;

import android.util.Log;

/**
 * Created by umme on 19/5/17.
 */

public class CustomLinkedList<T> {

    private Node<T> head;
    private Node<T> tail;


    public void add(T element){

        Node<T> nd = new Node<T>();
        nd.setValue(element);
        System.out.println("Adding: "+element);
        /**
         * check if the list is empty
         */
        if(head == null){
            //since there is only one element, both head and
            //tail points to the same object.
            head = nd;
            tail = nd;
        } else {
            //set current tail next link to new node
            tail.setNextRef(nd);
            //set tail as newly created node
            tail = nd;
        }
        traverse();
    }


    public void deleteFront(){

        if(head == null){
            System.out.println("Underflow...");
        }else {
            Node<T> tmp = head;
            head = tmp.getNextRef();
            if(head == null){
                tail = null;
            }
            System.out.println("Deleted: "+tmp.getValue());
        }
    }


    public void deleteTail(){

        if(head == null){
            System.out.println("Underflow......");
            tail = null;
        }else{
            Node<T> tmp = head;
            Node<T> refNode = null;
            System.out.println("Traversing to all nodes....");
            /**
             * Traverse till given element
             */
            while(true){
                if(tmp == null){
                    break;
                }
                if(tmp.compareTo(tail.getValue()) == 0){
                    //found the target node, add after this node
                    if(refNode == null){
                        deleteFront();
                    }else{
                        tail = refNode;
                    }
                    break;
                }
                refNode = tmp;
                tmp = tmp.getNextRef();
            }
            if(refNode != null){
                tmp = refNode.getNextRef();
                refNode.setNextRef(tmp.getNextRef());
//                if(refNode.getNextRef() == null){
//                    tail = refNode;
//                }
                System.out.println("Deleted: "+tmp.getValue());
            } else {
                System.out.println("Unable to find the given element.....");
            }
        }
        traverse();
    }

    public void deleteTargetted(Integer after){

        Node<T> tmp = head;
        Node<T> tmp1 = null;
        Node<T> refNode = null;
        System.out.println("Traversing to all nodes....");
        boolean xx = false;
        /**
         * Traverse till given element
         */
        while(true){
            if(tmp == null){
                break;
            }
            if(Integer.parseInt(tmp.getValue().toString()) > after){
                //found the target node, add after this node

                if(refNode == null){
                    deleteFront();
                    tmp = tmp.getNextRef();
                }
                if(tmp != null){
                    System.out.println("while true"+tmp.getValue());
                }


                xx = true;
                break;
            }else{
                xx = false;
            }
            refNode = tmp;
            tmp = tmp.getNextRef();
        }


        if(xx){

        } else{
            System.out.println("given element less than target......");
        }

//        System.out.println("Unable to find the given element..."+refNode.getValue());
        if(refNode != null){
            xx = false;
            tmp1 = refNode.getNextRef();
            if(refNode.getNextRef() == null){
                tail = refNode;
            }else{
                refNode.setNextRef(tmp1.getNextRef());
                System.out.println("Deleted: "+tmp1.getValue());
            }

        }
        if(tmp != null){
//                    xx = false;
            refNode = null;
            deleteTargetted(after);
        }else{
            traverse();
        }
    }

    public void traverse(){

        Node<T> tmp = head;
        System.out.println("======================Result======================");
        while(true){
            if(tmp == null){
                break;
            }
            System.out.println("List Values : "+tmp.getValue());
            tmp = tmp.getNextRef();
        }
    }

}

class Node<T> implements Comparable<T> {

    private T value;
    private Node<T> nextRef;

    public T getValue() {
        return value;
    }
    public void setValue(T value) {
        this.value = value;
    }
    public Node<T> getNextRef() {
        return nextRef;
    }
    public void setNextRef(Node<T> ref) {
        this.nextRef = ref;
    }

    @Override
    public int compareTo(T arg) {
        if(arg == this.value){
            return 0;
        } else {
            return 1;
        }
    }
}
